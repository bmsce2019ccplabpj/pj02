#include<stdio.h>
int main()
{
    int a,t,r,rev=0;
    printf("Enter a number to check whether it is palindrome or not\n");
    scanf("%d",&a);
    t=a;
    while(a!=0)
    {
        r=a%10;
        rev=(rev*10)+r;
        a=a/10;
    }
    if(t==rev)
    {
      printf("%d is equal to %d\n",t,rev);
      printf("%d is palindrome\n",t);
    }
    else
    {
        printf("%d is not equal to %d\n",t,rev);
        printf("%d is not palindrome\n",t);
    }
    return 0;
}