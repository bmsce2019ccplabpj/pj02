#include<stdio.h>
#include<math.h>
void dis(int x1,int x2,int y1,int y2)
{
    int x,y;
    float distance;
    x = x2-x1;
    y = y2-y1;
    distance = sqrt(pow(x,2)+pow(y,2));
    printf("The distance between points is %f\n",distance);
}
int main()
{   int x1,x2,y1,y2;
    printf("Enter co-ordinates of 2 points to calculate their distance\n");
    printf("Enter x1,x2,y1,y2\n");
    scanf("%d %d %d %d",&x1,&x2,&y1,&y2);
    dis(x1,x2,y1,y2);
	return 0;
}
